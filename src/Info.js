import React, {useReducer} from 'react';
import useInputs from './useInputs';

// 아래는 Custom Reducer로 생성
// function reducer(state, action) {
//   return {
//     ...state,
//     [action.name]: action.value
//   }
// }

const Info = () => {
  const [state, onChange] = useInputs({
    name: '',
    nickname: ''
  });

  const { name, nickname } = state;

  return (
    <div>
      <div>
        <input name='name' value={name} onChange={onChange} />
        <input name='nickname' value={nickname} onChange={onChange} />
      </div>
      <div>
        <b>이름 : {name}</b>
      </div>
      <div>
        <b>닉네임 : {nickname}</b>
      </div>
    </div>
  )
}


export default Info;
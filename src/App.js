import React from 'react';
import Average from './Average';
import Info from './Info';
// import Counter from './Counter';

const App = () => {
  return (
    <>
      <Average />
      <Info />
    </>
  );
}

export default App;
